package fr.aeroport.dom;

public class Siege {
    private String numAllee;
    private Integer numRangee;
    private Billet billet;

    private boolean reserve; // Ajout de l'attribut "reserve"

    public Siege(String numAllee, Integer numRangee) {
        this.numAllee = numAllee;
        this.numRangee = numRangee;
        this.reserve = false; // Par défaut, le siège n'est pas réservé
    }

    public boolean isReserve() {
        return reserve;
    }

    public String getNumAllee() {
        return numAllee;
    }

    public Integer getNumRangee() {
        return numRangee;
    }

    public Billet getBillet() {
        return billet;
    }

    public void setReserve(boolean reserve) {
        this.reserve = reserve;
    }

    public void setNumAllee(String numAllee) {
        this.numAllee = numAllee;
    }

    public void setNumRangee(Integer numRangee) {
        this.numRangee = numRangee;
    }

    public void setBillet(Billet billet) {
        this.billet = billet;
    }

    @Override
    public String toString() {
        return  numAllee + numRangee;
    }

}
