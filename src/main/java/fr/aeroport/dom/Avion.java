package fr.aeroport.dom;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;



public class Avion {
    private String modele;
    private char alleeMax;
    private int rangMax;

    public String getModele() {
        return modele;
    }

    private List<Siege> sieges;

    public Avion(String modele, char alleeMax, int rangMax) {
        this.modele = modele;
        this.alleeMax = alleeMax;
        this.rangMax = rangMax;
        this.sieges = genererSieges(alleeMax, rangMax);
    }




    private List<Siege> genererSieges(char alleeMax, int rangMax) {
        List<Siege> sieges = new ArrayList<>();
        for (char allee = 'A'; allee <= alleeMax; allee++) {
            for (int rang = 1; rang <= rangMax; rang++) {
                sieges.add(new Siege(String.valueOf(allee), rang)); // Convertir char en String
            }
        }
        return sieges;
    }

    public List<Siege> getSieges() {
        return sieges;
    }

    public Billet reserverSiege() {
        for (Siege siege : sieges) {
            if (siege.getBillet() == null) {
                Billet billet = new Billet(50);
                billet.setSiege(siege);
                siege.setBillet(billet);

                // Marquer le siège comme réservé
                // Cela permettra de le supprimer des sièges restants
                siege.setReserve(true);

                return billet;
            }
        }
        return null;
    }

    public void afficherSiegesRestants(Scanner scanner) {
        System.out.println("Sièges restants dans l'avion :");

        for (char allee = 'A'; allee <= alleeMax; allee++) {
            System.out.print("Allée " + allee + " Restant : ");
            for (int rang = 1; rang <= rangMax; rang++) {
                Siege siege = new Siege(String.valueOf(allee), rang);
                if (!siege.isReserve()) {
                    System.out.print(siege + " ");
                }
            }
            System.out.println();
        }
        System.out.println("Appuyez sur Entrée pour continuer...");
        scanner.nextLine();
    }



    public void afficherSiegesHublot() {
        System.out.println("Sièges côté hublot par allée :");

        System.out.print("Allée A : ");
        for (int rang = 1; rang <= rangMax; rang++) {
            Siege siege = new Siege("A", rang);
            if (siege.getBillet() == null) {
                System.out.print(siege + " ");
            }
        }

        System.out.println(); // Passer à la ligne suivante

        System.out.print("Allée I : ");
        for (int rang = 1; rang <= rangMax; rang++) {
            Siege siege = new Siege("I", rang);
            if (siege.getBillet() == null) {
                System.out.print(siege + " ");
            }
        }

        System.out.println(); // Passer à la ligne suivante
    }


    private boolean estSiegeHublot(String numAllee, int numRangee, int rangMax) {
        return numAllee.equals("A") || numAllee.equals("I") || numRangee == 1 || numRangee == rangMax;
    }

    public void afficherSiegesOccupesEtRestants() {
        int siegesOccupes = 0;

        for (Siege siege : sieges) {
            if (siege.getBillet() != null) {
                siegesOccupes++;
            }
        }

        int totalSieges = sieges.size();
        int siegesRestants = totalSieges - siegesOccupes;

        System.out.println("Sièges occupés : " + siegesOccupes + " / " + totalSieges);
        System.out.println("Sièges restants : " + siegesRestants + " / " + totalSieges);
    }
}
