package fr.aeroport.dom;

import java.util.Date;

public class Billet {
    private Integer prix;
    private Siege siege;
    private String numeroBillet; // Numéro unique du billet
    private String nomPassager; // Nom du passager
    private Date dateEmission; // Date d'émission du billet
    private Integer numeroVol; // Numéro de vol associé

    public Billet(Integer prix, String numeroBillet, String nomPassager, Date dateEmission, Integer numeroVol) {
        this.prix = prix;
        this.numeroBillet = numeroBillet;
        this.nomPassager = nomPassager;
        this.dateEmission = dateEmission;
        this.numeroVol = numeroVol;
    }

    public Billet(int prix) {
    }

    public Integer getPrix() {
        return prix;
    }

    public Siege getSiege() {
        return siege;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public void setSiege(Siege siege) {
        this.siege = siege;
    }

    public String getNumeroBillet() {
        return numeroBillet;
    }

    public String getNomPassager() {
        return nomPassager;
    }

    public Date getDateEmission() {
        return dateEmission;
    }

    public Integer getNumeroVol() {
        return numeroVol;
    }

    // Ajoutez les setters pour les attributs supplémentaires si nécessaire
}
