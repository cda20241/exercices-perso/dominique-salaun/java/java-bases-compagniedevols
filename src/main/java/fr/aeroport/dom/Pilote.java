package fr.aeroport.dom;

import java.util.Random;

public class Pilote {
    private String nom;
    private String grade;
    private int age;  // Vous pouvez ajouter d'autres attributs spécifiques au pilote ici

    public Pilote(String grade, String nom, int age) {
        this.grade = grade;
        this.nom = nom;
        this.age = age;
    }

    // Ajoutez une méthode statique pour générer un nom de pilote aléatoire
    public static Pilote genererPiloteAleatoire() {
        String[] grades = {"Commandant de bord","Officier pilote de ligne","Pilote de ligne"};
        String[] prenoms = {"John", "Jane", "Michael", "Emily", "David", "Olivia"};
        String[] nomsDeFamille = {"Smith", "Johnson", "Brown", "Davis", "Wilson", "Martinez"};
        Random random = new Random();
        String grade = grades[random.nextInt(grades.length)];
        String prenom = prenoms[random.nextInt(prenoms.length)];
        String nomDeFamille = nomsDeFamille[random.nextInt(nomsDeFamille.length)];
        String nomComplet = prenom + " " + nomDeFamille;

        // Générez un âge aléatoire entre 25 et 60 (vous pouvez ajuster ces valeurs)
        int age = random.nextInt(36) + 25;

        return new Pilote(grade,nomComplet, age);
    }

    @Override
    public String toString() {
        return grade+", " + nom + ", age= " + age;
    }
}