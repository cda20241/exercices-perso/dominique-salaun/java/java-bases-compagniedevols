package fr.aeroport.dom;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Compagnie compagnie = null;
        Avion avion = null;

        boolean continuer = true;

        while (continuer) {
            System.out.println("Menu :");
            System.out.println("1. Créer une compagnie");
            System.out.println("2. Créer un vol");
            System.out.println("3. Réserver une place");
            System.out.println("4. Afficher les places occupées et les places restantes");
            System.out.println("5. Voir les places côté hublot");
            System.out.println("6. Voir les places restantes");
            System.out.println("7. Voir les vols");
            System.out.println("8. Quitter");

            System.out.print("Choisissez une option (1/2/3/4/5/6/7/8) : ");

            int choix;

            while (true) {
                try {
                    choix = Integer.parseInt(scanner.nextLine());
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("Option invalide. Veuillez entrer un chiffre.");
                }
            }

            switch (choix) {
                case 1:
                    System.out.println("Nom de la Compagnie :");
                    String nomDeCompagnie = scanner.nextLine();
                    compagnie = new Compagnie(nomDeCompagnie);
                    break;

                case 2:
                    if (compagnie != null) {
                        avion = compagnie.creerVol().getAvion();
                    } else {
                        System.out.println("Veuillez créer une compagnie avant de créer un vol.");
                    }
                    break;


                case 3:
                    if (avion != null) {
                        avion.reserverSiege();
                    } else {
                        System.out.println("Veuillez créer un vol avant de réserver une place.");
                    }
                    break;

                case 4:
                    if (avion != null) {
                        avion.afficherSiegesOccupesEtRestants();
                    } else {
                        System.out.println("Veuillez créer un vol pour afficher les places occupées et restantes.");
                    }
                    break;

                case 5:
                    if (avion != null) {
                        avion.afficherSiegesHublot();
                    } else {
                        System.out.println("Veuillez créer un vol pour voir les places côté hublot.");
                    }
                    break;

                case 6:
                    if (avion != null) {
                        avion.afficherSiegesRestants(scanner);
                    } else {
                        System.out.println("Veuillez créer un vol pour voir les places restantes.");
                    }
                    break;

                case 7:
                    if (compagnie != null) {
                        compagnie.listerVols();
                    } else {
                        System.out.println("Veuillez créer une compagnie pour voir les vols.");
                    }
                    break;

                case 8:
                    continuer = false;
                    break;

                default:
                    System.out.println("Option invalide. Veuillez choisir une option valide.");
            }
        }

        System.out.println("Merci d'avoir utilisé notre service de réservation de sièges !");
    }
}
