package fr.aeroport.dom;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Compagnie {
    private final String nom;
    private static List<Vol> vols;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
    private static final SimpleDateFormat heureFormat = new SimpleDateFormat("HH:mm");
    private static final Scanner scanner = new Scanner(System.in);
    private static int numVol = 1; // Numéro de vol initial

    public Compagnie(String nom) {
        this.nom = nom;
        vols = new ArrayList<>();
    }

    public Vol creerVol() {
        System.out.print("Ville de départ : ");
        String villeDepart = scanner.nextLine();

        System.out.print("Ville d'arrivée : ");
        String villeArrivee = scanner.nextLine();

        System.out.print("Modèle de l'avion : ");
        String modeleAvion = scanner.nextLine();

        System.out.print("Entrez la lettre maximale de l'allée (de A à I) : ");
        char alleeMax = Character.toUpperCase(scanner.next().charAt(0));

        System.out.print("Entrez le nombre maximal de rangées : ");
        int rangMax = scanner.nextInt();
        scanner.nextLine();

        Avion avion = new Avion(modeleAvion, alleeMax, rangMax);
        Compagnie compagnie = new Compagnie(nom);

        Date dateDepart = demanderDate("Date de départ (ddMMyyyy) : ");
        Date dateArrivee = demanderDate("Date d'arrivée (ddMMyyyy) : ");
        Date heureDepart = demanderHeure("Heure de départ (HH:mm) : ");
        Date heureArrivee = demanderHeure("Heure d'arrivée (HH:mm) : ");

        Vol nouveauVol = new Vol(
                dateDepart,
                dateArrivee,
                villeDepart,
                villeArrivee,
                heureDepart,
                heureArrivee
        );

        nouveauVol.setAvion(avion);
        nouveauVol.setPilote(Pilote.genererPiloteAleatoire());
        vols.add(nouveauVol);
        nouveauVol.setCompagnie(this);
        nouveauVol.setNumeroVol(numVol);
        numVol++; // Incrémente le numéro de vol après chaque création

        System.out.println("Vol créé avec succès :");
        System.out.println("Numéro de vol : " + nouveauVol.getNumVol());
        System.out.println("Ville de départ : " + villeDepart);
        System.out.println("Ville d'arrivée : " + villeArrivee);
        System.out.println("Date de départ : " + dateFormat.format(dateDepart));
        System.out.println("Date d'arrivée : " + dateFormat.format(dateArrivee));
        System.out.println("Heure de départ : " + heureFormat.format(heureDepart));
        System.out.println("Heure d'arrivée : " + heureFormat.format(heureArrivee));
        System.out.println("Avion : " + avion.getModele());
        System.out.println("Pilote : " + nouveauVol.getPilote());
        System.out.println("Vol " + nouveauVol.getNumVol() + " créé avec succès !");

        return nouveauVol;
    }

    public static Date demanderDate(String message) {
        Date date = null;
        while (date == null) {
            try {
                System.out.print(message);
                String dateString = scanner.nextLine();
                date = dateFormat.parse(dateString);
                if (date == null) {
                    System.out.println("La date ne peut pas être nulle. Veuillez entrer une date valide.");
                }
            } catch (ParseException e) {
                System.out.println("Format de date invalide. Veuillez utiliser le format ddMMyyyy.");
            }
        }
        return date;
    }

    public static Date demanderHeure(String message) {
        SimpleDateFormat heureFormat = new SimpleDateFormat("HH:mm");
        Date heure = null;

        while (heure == null) {
            try {
                System.out.print(message);
                String heureString = scanner.nextLine();
                heure = heureFormat.parse(heureString);
                if (heure == null) {
                    System.out.println("Heure invalide. Veuillez entrer une heure au format HH:mm.");
                }
            } catch (ParseException e) {
                System.out.println("Format d'heure invalide. Utilisez le format HH:mm.");
            }
        }

        return heure;
    }

    public static void listerVols() {
        System.out.println("Liste des vols :");
        for (Vol vol : vols) {
            if (vol.getDateDepart() != null && vol.getDateArrivee() != null
                    && vol.getHeureDepart() != null && vol.getHeureArrivee() != null) {

                String volInfo = "Numéro de vol : " + vol.getNumVol() + " | " +
                        "Ville de départ : " + vol.getVilleDepart() + " | " +
                        "Ville d'arrivée : " + vol.getVilleArrivee() + " | " +
                        "Date de départ : " + dateFormat.format(vol.getDateDepart()) + " | " +
                        "Date d'arrivée : " + dateFormat.format(vol.getDateArrivee()) + " | " +
                        "Heure de départ : " + heureFormat.format(vol.getHeureDepart()) + " | " +
                        "Heure d'arrivée : " + heureFormat.format(vol.getHeureArrivee()) + " | " +
                        "Avion : " + vol.getAvion().getModele() + " | " +
                        "Pilote : " + vol.getPilote();

                System.out.println(volInfo);
            } else {
                System.out.println("Erreur : Les informations de date ou heure sont manquantes pour le vol " + vol.getNumVol());
            }
        }
    }
}
